#include <stdio.h>
int input();
int check(int);
void output(int);

int main()
{
    int y;
    y = input();
    output(check(y));
    return 0;
}
int input()
{
    int y;
    printf("Enter the year to be checked for a leap year\n");
    scanf("%d",&y);
    return y;
}
int check(int y)
{
    if(y%4==0)
        return 0;
    else
        return 1;
}
void output(int c)
{
    if(c==0)
        printf("Entered year is a leap year.\n");
    else
        printf("Entered year is not a leap year.\n");
}
    