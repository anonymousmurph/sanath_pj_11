#include <stdio.h>
int input();
int check(int);
void output(int);

int main()
{
    int a,c;
    a = input();
    c = check(a);
    output(c);
    return 0;
}
int input()
{
    int a;
    printf("Enter the number to be checked\n");
    scanf("%d",&a);
    return a;
}
int check(int a)
{
    if(a%2==0)
        return 0;
    else
        return 1;
}
void output(int c)
{
    if(c==0)
        printf("The number entered is even\n");
    else
        printf("The number entered is odd\n");
}
