#include<stdio.h>
int input()
{
    int num;
    printf("Enter a number:\n");
    scanf("%d",&num);
    return num;
}
void compute(int num,int *p, int *n, float *avg)
{
	int count=0,sum=0;
	*p=0, *n=0;
	while(num!=0)
	{
		if(num>0)
			{
				*p=*p+1;
				sum+=num;
				count++;
			}
		else
			{
				*n=*n+1;
			}
		printf("Enter a integer or 0 to exit:\n");
		scanf("%d",&num);
	}
	*avg=(float)sum/count;
	return ;
}

void output(int p,int n, float avg)
{
	printf("Count of positive numbers = %d\n",p);
	printf("Count of negative numbers = %d\n",n);
	printf("Average of all the positive numbers = %.2f",avg);
}
int main()
{
	int num,p,n;
	float avg;
	num=input();
	compute(num,&p,&n,&avg);
	output(p,n,avg);
    return 0;
}
