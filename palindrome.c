#include<stdio.h>
#include<stdlib.h>
int main()
{
    int n=0,d,rev=0,t;
    printf("Enter a number\n");
    scanf("%d", &n);
    t=n;
    while(n!=0)
    {
        d=n%10;
        rev=rev*10+d;
        n=n/10;
    }
    if(t==rev)
    {
    printf("The number is a palindrome.\n");
    }
    else
    {
    printf("The number is not a palindrome.\n");
    }
    return 0;
}
